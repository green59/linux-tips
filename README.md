# Linux Tips

## Project information
Collections of various tips for using Linux distribution and more.

As for comprehensive manual, go to https://usergreen.github.io/

## License
It is licensed under GFDL: https://www.gnu.org/licenses/fdl-1.3.txt

## Project status
It is now active. (since January 29, 2024)
