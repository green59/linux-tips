SSH 公開鍵の作成と登録

UNIX/Linux 環境で OpenSSH を利用する場合、「ssh-keygen」コマンドで SSH 公開鍵を作成できます。

ssh-keygen コマンドを実行すると鍵の保存ディレクトリを尋ねられますが、何も入力せずに Enter キーを押します。続いてパスフレーズ（パスワード）の入力が求められるので、適当なパスフレーズを入力して Enter キーを押します。確認のため、入力したパスフレーズの再入力を求められるので、同じものを再入力します。

以上でホームディレクトリに「.ssh」というディレクトリが作成され、このディレクトリ内に作成された公開鍵が保存されます。「id_rsa.pub」というファイルが公開鍵、「id_rsa」というファイルが秘密鍵となります。

$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/c/Users/hirom/.ssh/id_rsa):　←何も入力せずに Enter キーを押す
Created directory '/c/Users/hirom/.ssh'.
Enter passphrase (empty for no passphrase):　←パスフレーズを入力して Enter キーを押す
Enter same passphrase again:　　　　　　　　←再度同じパスフレーズを入力して Enter キーを押す
Your identification has been saved in /c/Users/hirom/.ssh/id_rsa.
Your public key has been saved in /c/Users/hirom/.ssh/id_rsa.pub.
The key fingerprint is:
57:53:fa:62:a2:78:cf:da:5f:e4:9d:e1:9d:c5:ef:2b hirom@HIROM-OPT790

続いて作成した公開鍵（id_rsa.pub）をエディタなどで開き、その内容をコピーします。このとき、不要な改行などが入らないように注意してください。


参照サイト：

https://ja.osdn.net/docs/Register_PublicKey

https://web.archive.org/web/20240205084517/https://ja.osdn.net/docs/Register_PublicKey

