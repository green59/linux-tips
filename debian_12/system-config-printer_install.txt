
root@debian:/home/user# apt install system-config-printer
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following additional packages will be installed:
  avahi-utils cups-pk-helper gir1.2-handy-1 gir1.2-notify-0.7
  gir1.2-packagekitglib-1.0 gir1.2-polkit-1.0 gir1.2-secret-1
  libpackagekit-glib2-18 python3-cups python3-cupshelpers python3-dbus
  python3-smbc system-config-printer-common system-config-printer-udev
Suggested packages:
  python-dbus-doc gnome-software
The following NEW packages will be installed:
  avahi-utils cups-pk-helper gir1.2-handy-1 gir1.2-notify-0.7
  gir1.2-packagekitglib-1.0 gir1.2-polkit-1.0 gir1.2-secret-1
  libpackagekit-glib2-18 python3-cups python3-cupshelpers python3-dbus
  python3-smbc system-config-printer system-config-printer-common
  system-config-printer-udev
0 upgraded, 15 newly installed, 0 to remove and 0 not upgraded.
Need to get 1,726 kB of archives.
After this operation, 8,698 kB of additional disk space will be used.
Do you want to continue? [Y/n] 

