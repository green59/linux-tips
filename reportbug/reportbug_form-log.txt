user@debian:~$ reportbug
Please enter the name of the package in which you have found a problem, or type
'other' to report a more general problem. If you don't know what package the
bug is in, please contact debian-user@lists.debian.org for assistance.
> cinnamon
*** Welcome to reportbug.  Use ? for help at prompts. ***
Note: bug reports are publicly archived (including the email address of the submitter).
Detected character set: UTF-8
Please change your locale if this is incorrect.

Using 'Green <usergreen@xxxx.yyy.zz>' as your from address.
Getting status for cinnamon...
Which of the following packages is the bug in?

1 cinnamon         Innovative and comfortable desktop

2 cinnamon-common  Innovative and comfortable desktop (Common data files)

3 cinnamon-doc     Innovative and comfortable desktop (Documentation)

Select one of these packages: 1
Please enter the version of the package this report applies to (blank OK)
> 4.8.6
Will send report to Debian (per lsb_release).
Querying Debian BTS for reports on cinnamon (source)...
Unable to connect to Debian BTS (error: "timeout('timed out')"); continue [y|N|?]? y
No bug reports found.

Briefly describe the problem (max. 100 characters allowed). This will be the
bug email subject, so keep the summary as concise as possible, for example:
"fails to send email" or "does not start with -q option specified" (enter
Ctrl+c to exit reportbug without reporting a bug).
> Square icon on the title bar is too small
Rewriting subject to 'cinnamon: Square icon on the title bar is too small'
Removing release critical severities, since running in 'novice' mode.
How would you rate the severity of this problem or report?

1 important  a bug which has a major effect on the usability of a package,
             without rendering it completely unusable to everyone.

2 normal     a bug that does not undermine the usability of the whole package;
             for example, a problem with a particular option or menu item.

3 minor      things like spelling mistakes and other minor cosmetic errors that
             do not affect the core functionality of the package.

4 wishlist   suggestions and requests for new features.

Please select a severity level: [normal] 2
Spawning sensible-editor...
Report will be sent to Debian Bug Tracking System <submit@bugs.debian.org>
Submit this report on cinnamon (e to edit) [Y|n|a|c|e|i|l|m|p|q|d|t|s|?]? Y
Saving a backup of the report at /tmp/reportbug-cinnamon-backup-20220928-4177-czyylqgc
Connecting to reportbug.debian.org via SMTP...

Bug report submitted to: Debian Bug Tracking System <submit@bugs.debian.org>
Copies will be sent after processing to:
  usergreen@xxxx.yyy.zz

If you want to provide additional information, please wait to receive the bug
tracking number via email; you may then send any extra information to
n@bugs.debian.org (e.g. nnnnnn@bugs.debian.org), where n is the bug number.
Normally you will receive an acknowledgement via email including the bug report
number within an hour; if you haven't received a confirmation, then the bug
reporting process failed at some point (reportbug or MTA failure, BTS
maintenance, etc.).
user@debian:~$ 

