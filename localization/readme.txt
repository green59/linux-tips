[How to update translation of your installed application]

This ja.mo file will fix typos in Japanese in MATE Screenshot. Overwrite the existing app's .mo file with updated .mo file from the commandline as below:

[Usage]

root@mx1:/home/demo/# cp ja.mo /usr/share/locale/ja/LC_MESSAGES/mate-utils.mo

Note: "ja" is a locale code for Japanese language. 

END.
