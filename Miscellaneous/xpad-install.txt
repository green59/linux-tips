demo@mx1:~
$ LANG=c apt show xpad
Package: xpad
Version: 5.8.0-1
Priority: optional
Section: x11
Maintainer: Jeroen Ploemen <jcfp@debian.org>
Installed-Size: 468 kB
Depends: libayatana-appindicator3-1 (>= 0.4.90), libc6 (>= 2.15), libglib2.0-0 (>= 2.55.1), libgtk-3-0 (>= 3.21.5), libgtksourceview-4-0 (>= 3.10.1), libice6 (>= 1:1.0.0), libpango-1.0-0 (>= 1.14.0), libsm6
Homepage: https://launchpad.net/xpad
Tag: interface::graphical, interface::x11, role::program, scope::utility,
 uitoolkit::gtk, use::organizing, works-with::pim, x11::application
Download-Size: 110 kB
APT-Sources: http://deb.debian.org/debian bookworm/main amd64 Packages
Description: sticky note application for X
 This program consists of independent pad windows, each is basically a text box
 in which notes can be written. Xpad attempts to emulate the look of postit
 notes, although the look of the notes can be customized any way you like.

demo@mx1:~
$ LANG=c su
Password: 
root@mx1:/home/demo# LANG=c apt install xpad
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following additional packages will be installed:
  libayatana-appindicator3-1 libdbusmenu-glib4 libdbusmenu-gtk3-4
The following NEW packages will be installed:
  libayatana-appindicator3-1 libdbusmenu-glib4 libdbusmenu-gtk3-4 xpad
0 upgraded, 4 newly installed, 0 to remove and 1 not upgraded.
Need to get 206 kB of archives.
After this operation, 808 kB of additional disk space will be used.
Do you want to continue? [Y/n] 
