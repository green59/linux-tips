How To Edit PDF Files In Linux In The Easiest Way Possible
https://itsfoss.com/edit-pdf-files-ubuntu-linux/

You can go to the specific pages and edit the PDF file there. Just click on the text which you want to edit.

Once you are done with the edits, instead of saving the file (using Ctrl+S) option, click on Export to PDF button.

