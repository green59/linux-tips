# apt install apparmor apparmor-utils
パッケージリストを読み込んでいます... 完了
依存関係ツリーを作成しています                
状態情報を読み取っています... 完了
以下の追加パッケージがインストールされます:
  libapparmor-perl python3-apparmor python3-libapparmor
提案パッケージ:
  apparmor-profiles apparmor-profiles-extra vim-addon-manager
以下のパッケージが新たにインストールされます:
  apparmor apparmor-utils libapparmor-perl python3-apparmor
  python3-libapparmor
アップグレード: 0 個、新規インストール: 5 個、削除: 0 個、保留: 0 個。
919 kB のアーカイブを取得する必要があります。
この操作後に追加で 3,027 kB のディスク容量が消費されます。
続行しますか? [Y/n]

# mkdir -p /etc/default/grub.d

# GRUB_CMDLINE_LINUX_DEFAULT="$GRUB_CMDLINE_LINUX_DEFAULT apparmor=1 security=apparmor"

# update-grub
Generating grub configuration file ...
Found background image: /usr/share/images/desktop-base/desktop-grub.png
Linux イメージを見つけました: /boot/vmlinuz-4.9.0-11-amd64
Found initrd image: /boot/initrd.img-4.9.0-11-amd64
Linux イメージを見つけました: /boot/vmlinuz-4.9.0-9-amd64
Found initrd image: /boot/initrd.img-4.9.0-9-amd64
Found Windows Recovery Environment on /dev/sda1
Found Windows 7 on /dev/sda2
完了

Then reboot.

------------------------------------------------------
# apt install auditd
パッケージリストを読み込んでいます... 完了
依存関係ツリーを作成しています                
状態情報を読み取っています... 完了
以下の追加パッケージがインストールされます:
  libauparse0
提案パッケージ:
  audispd-plugins
以下のパッケージが新たにインストールされます:
  auditd libauparse0
アップグレード: 0 個、新規インストール: 2 個、削除: 0 個、保留: 0 個。
259 kB のアーカイブを取得する必要があります。
この操作後に追加で 781 kB のディスク容量が消費されます。
続行しますか? [Y/n]

# apt install apparmor-profiles apparmor-profiles-extra
パッケージリストを読み込んでいます... 完了
依存関係ツリーを作成しています                
状態情報を読み取っています... 完了
以下のパッケージが新たにインストールされます:
  apparmor-profiles apparmor-profiles-extra
アップグレード: 0 個、新規インストール: 2 個、削除: 0 個、保留: 0 個。
89.7 kB のアーカイブを取得する必要があります。
この操作後に追加で 395 kB のディスク容量が消費されます。

# apt install apparmor-notify
パッケージリストを読み込んでいます... 完了
依存関係ツリーを作成しています                
状態情報を読み取っています... 完了
以下の追加パッケージがインストールされます:
  libnotify-bin
以下のパッケージが新たにインストールされます:
  apparmor-notify libnotify-bin
アップグレード: 0 個、新規インストール: 2 個、削除: 0 個、保留: 0 個。
75.8 kB のアーカイブを取得する必要があります。
この操作後に追加で 139 kB のディスク容量が消費されます。
続行しますか? [Y/n]

