root@mx1:/home/demo# apt -s install openshot-qt
パッケージリストを読み込んでいます... 完了
依存関係ツリーを作成しています... 完了        
状態情報を読み取っています... 完了        
以下の追加パッケージがインストールされます:
  gdal-data gdal-plugins libaec0 libarmadillo11 libarpack2 libbabl-0.1-0
  libblosc1 libcfitsio10 libcharls2 libfreexl1 libfyba0 libgdal32 libgdcm3.0
  libgeos-c1v5 libgeos3.11.1 libgeotiff5 libhdf4-0-alt libhdf5-103-1
  libhdf5-hl-100 libjsoncpp25 libkmlbase1 libkmldom1 libkmlengine1 liblept5
  libmagick++-6.q16-8 libmariadb3 libnetcdf19 libodbc2 libodbcinst2 libogdi4.1
  libopencv-calib3d406 libopencv-contrib406 libopencv-dnn406
  libopencv-features2d406 libopencv-flann406 libopencv-highgui406
  libopencv-imgcodecs406 libopencv-ml406 libopencv-objdetect406
  libopencv-video406 libopenshot-audio9 libopenshot23 libpq5 libproj25
  libqhull-r8.0 libqt5opengl5 libqt5webengine5 librttopo1 libsocket++1
  libspatialite7 libsuperlu5 libsz2 libtesseract5 liburiparser1 libxerces-c3.2
  mariadb-common mysql-common proj-data python3-cffi-backend
  python3-defusedxml python3-openshot python3-py python3-pyqt5.qtopengl
  python3-pyqt5.qtsvg python3-pyqt5.qtwebchannel python3-pyqt5.qtwebengine
  python3-sentry-sdk python3-zmq unixodbc-common
提案パッケージ:
  geotiff-bin gdal-bin libgeotiff-epsg libhdf4-doc libhdf4-alt-dev hdf4-tools
  odbc-postgresql tdsodbc ogdi-bin proj-bin openshot-qt-doc subversion
  python3-pytest
推奨パッケージ:
  proj-bin
以下のパッケージが新たにインストールされます:
  gdal-data gdal-plugins libaec0 libarmadillo11 libarpack2 libbabl-0.1-0
  libblosc1 libcfitsio10 libcharls2 libfreexl1 libfyba0 libgdal32 libgdcm3.0
  libgeos-c1v5 libgeos3.11.1 libgeotiff5 libhdf4-0-alt libhdf5-103-1
  libhdf5-hl-100 libjsoncpp25 libkmlbase1 libkmldom1 libkmlengine1 liblept5
  libmagick++-6.q16-8 libmariadb3 libnetcdf19 libodbc2 libodbcinst2 libogdi4.1
  libopencv-calib3d406 libopencv-contrib406 libopencv-dnn406
  libopencv-features2d406 libopencv-flann406 libopencv-highgui406
  libopencv-imgcodecs406 libopencv-ml406 libopencv-objdetect406
  libopencv-video406 libopenshot-audio9 libopenshot23 libpq5 libproj25
  libqhull-r8.0 libqt5opengl5 libqt5webengine5 librttopo1 libsocket++1
  libspatialite7 libsuperlu5 libsz2 libtesseract5 liburiparser1 libxerces-c3.2
  mariadb-common mysql-common openshot-qt proj-data python3-cffi-backend
  python3-defusedxml python3-openshot python3-py python3-pyqt5.qtopengl
  python3-pyqt5.qtsvg python3-pyqt5.qtwebchannel python3-pyqt5.qtwebengine
  python3-sentry-sdk python3-zmq unixodbc-common
アップグレード: 0 個、新規インストール: 70 個、削除: 0 個、保留: 0 個。
Inst gdal-data (3.6.2+dfsg-1 Debian:12.0/stable [all])
Inst gdal-plugins (3.6.2+dfsg-1+b2 Debian:12.0/stable [amd64])
Inst libaec0 (1.0.6-1+b1 Debian:12.0/stable [amd64])
Inst libarpack2 (3.8.0-3 Debian:12.0/stable [amd64])
Inst libsuperlu5 (5.3.0+dfsg1-2+b1 Debian:12.0/stable [amd64])
Inst libarmadillo11 (1:11.4.2+dfsg-1 Debian:12.0/stable [amd64])
Inst libbabl-0.1-0 (1:0.1.98-1+b1 Debian:12.0/stable [amd64])
Inst libblosc1 (1.21.3+ds-1 Debian:12.0/stable [amd64])
Inst libcfitsio10 (4.2.0-3 Debian:12.0/stable [amd64])
Inst libcharls2 (2.4.1-1 Debian:12.0/stable [amd64])
Inst libfreexl1 (1.0.6-2 Debian:12.0/stable [amd64])
Inst libfyba0 (4.1.1-8 Debian:12.0/stable [amd64])
Inst libgeos3.11.1 (3.11.1-1 Debian:12.0/stable [amd64])
Inst libgeos-c1v5 (3.11.1-1 Debian:12.0/stable [amd64])
Inst proj-data (9.1.1-1 Debian:12.0/stable [all])
Inst libproj25 (9.1.1-1+b1 Debian:12.0/stable [amd64])
Inst libgeotiff5 (1.7.1-2+b1 Debian:12.0/stable [amd64])
Inst libhdf4-0-alt (4.2.15-5 Debian:12.0/stable [amd64])
Inst libsz2 (1.0.6-1+b1 Debian:12.0/stable [amd64])
Inst libhdf5-103-1 (1.10.8+repack1-1 Debian:12.0/stable [amd64])
Inst liburiparser1 (0.9.7+dfsg-2 Debian:12.0/stable [amd64])
Inst libkmlbase1 (1.3.0-10 Debian:12.0/stable [amd64])
Inst libkmldom1 (1.3.0-10 Debian:12.0/stable [amd64])
Inst libkmlengine1 (1.3.0-10 Debian:12.0/stable [amd64])
Inst mysql-common (5.8+1.1.0 Debian:12.0/stable [all])
Inst mariadb-common (1:10.11.3-1 Debian:12.0/stable [all])
Inst libmariadb3 (1:10.11.3-1 Debian:12.0/stable [amd64])
Inst libhdf5-hl-100 (1.10.8+repack1-1 Debian:12.0/stable [amd64])
Inst libnetcdf19 (1:4.9.0-3+b1 Debian:12.0/stable [amd64])
Inst libodbc2 (2.3.11-2 Debian:12.0/stable [amd64])
Inst unixodbc-common (2.3.11-2 Debian:12.0/stable [all])
Inst libodbcinst2 (2.3.11-2 Debian:12.0/stable [amd64])
Inst libogdi4.1 (4.1.0+ds-6 Debian:12.0/stable [amd64])
Inst libpq5 (15.3-0+deb12u1 Debian:12.0/stable [amd64])
Inst libqhull-r8.0 (2020.2-5 Debian:12.0/stable [amd64])
Inst librttopo1 (1.1.0-3 Debian:12.0/stable [amd64])
Inst libspatialite7 (5.0.1-3 Debian:12.0/stable [amd64])
Inst libxerces-c3.2 (3.2.4+debian-1 Debian:12.0/stable [amd64])
Inst libgdal32 (3.6.2+dfsg-1+b2 Debian:12.0/stable [amd64])
Inst libsocket++1 (1.12.13+git20131030.5d039ba-1+b1 Debian:12.0/stable [amd64])
Inst libgdcm3.0 (3.0.21-1 Debian:12.0/stable [amd64])
Inst libjsoncpp25 (1.9.5-4 Debian:12.0/stable [amd64])
Inst liblept5 (1.82.0-3+b3 Debian:12.0/stable [amd64])
Inst libmagick++-6.q16-8 (8:6.9.11.60+dfsg-1.6 Debian:12.0/stable [amd64])
Inst libopencv-flann406 (4.6.0+dfsg-12 Debian:12.0/stable [amd64])
Inst libopencv-features2d406 (4.6.0+dfsg-12 Debian:12.0/stable [amd64])
Inst libopencv-calib3d406 (4.6.0+dfsg-12 Debian:12.0/stable [amd64])
Inst libopencv-dnn406 (4.6.0+dfsg-12 Debian:12.0/stable [amd64])
Inst libqt5opengl5 (5.15.8+dfsg-11 Debian:12.0/stable [amd64])
Inst libopencv-highgui406 (4.6.0+dfsg-12 Debian:12.0/stable [amd64])
Inst libopencv-imgcodecs406 (4.6.0+dfsg-12 Debian:12.0/stable [amd64])
Inst libopencv-ml406 (4.6.0+dfsg-12 Debian:12.0/stable [amd64])
Inst libopencv-objdetect406 (4.6.0+dfsg-12 Debian:12.0/stable [amd64])
Inst libopencv-video406 (4.6.0+dfsg-12 Debian:12.0/stable [amd64])
Inst libtesseract5 (5.3.0-2 Debian:12.0/stable [amd64])
Inst libopencv-contrib406 (4.6.0+dfsg-12 Debian:12.0/stable [amd64])
Inst libopenshot-audio9 (0.3.1+dfsg1-0.1~mx23+1 MX repository:23.0/mx [amd64])
Inst libopenshot23 (0.3.1+dfsg1-0.1~mx23+1 MX repository:23.0/mx [amd64])
Inst libqt5webengine5 (5.15.13+dfsg-1~deb12u1 Debian:12.0/stable [amd64])
Inst python3-sentry-sdk (1.9.10-2 Debian:12.0/stable [all])
Inst python3-defusedxml (0.7.1-2 Debian:12.0/stable [all])
Inst python3-openshot (0.3.1+dfsg1-0.1~mx23+1 MX repository:23.0/mx [amd64])
Inst python3-pyqt5.qtopengl (5.15.9+dfsg-1 Debian:12.0/stable [amd64])
Inst python3-pyqt5.qtsvg (5.15.9+dfsg-1 Debian:12.0/stable [amd64])
Inst python3-pyqt5.qtwebchannel (5.15.9+dfsg-1 Debian:12.0/stable [amd64])
Inst python3-pyqt5.qtwebengine (5.15.6-1 Debian:12.0/stable [amd64])
Inst python3-cffi-backend (1.15.1-5+b1 Debian:12.0/stable [amd64])
Inst python3-py (1.11.0-1 Debian:12.0/stable [all])
Inst python3-zmq (24.0.1-4+b1 Debian:12.0/stable [amd64])
Inst openshot-qt (3.1.0+dfsg1-0.1~mx23+1 MX repository:23.0/mx [all])
Conf gdal-data (3.6.2+dfsg-1 Debian:12.0/stable [all])
Conf gdal-plugins (3.6.2+dfsg-1+b2 Debian:12.0/stable [amd64])
Conf libaec0 (1.0.6-1+b1 Debian:12.0/stable [amd64])
Conf libarpack2 (3.8.0-3 Debian:12.0/stable [amd64])
Conf libsuperlu5 (5.3.0+dfsg1-2+b1 Debian:12.0/stable [amd64])
Conf libarmadillo11 (1:11.4.2+dfsg-1 Debian:12.0/stable [amd64])
Conf libbabl-0.1-0 (1:0.1.98-1+b1 Debian:12.0/stable [amd64])
Conf libblosc1 (1.21.3+ds-1 Debian:12.0/stable [amd64])
Conf libcfitsio10 (4.2.0-3 Debian:12.0/stable [amd64])
Conf libcharls2 (2.4.1-1 Debian:12.0/stable [amd64])
Conf libfreexl1 (1.0.6-2 Debian:12.0/stable [amd64])
Conf libfyba0 (4.1.1-8 Debian:12.0/stable [amd64])
Conf libgeos3.11.1 (3.11.1-1 Debian:12.0/stable [amd64])
Conf libgeos-c1v5 (3.11.1-1 Debian:12.0/stable [amd64])
Conf proj-data (9.1.1-1 Debian:12.0/stable [all])
Conf libproj25 (9.1.1-1+b1 Debian:12.0/stable [amd64])
Conf libgeotiff5 (1.7.1-2+b1 Debian:12.0/stable [amd64])
Conf libhdf4-0-alt (4.2.15-5 Debian:12.0/stable [amd64])
Conf libsz2 (1.0.6-1+b1 Debian:12.0/stable [amd64])
Conf libhdf5-103-1 (1.10.8+repack1-1 Debian:12.0/stable [amd64])
Conf liburiparser1 (0.9.7+dfsg-2 Debian:12.0/stable [amd64])
Conf libkmlbase1 (1.3.0-10 Debian:12.0/stable [amd64])
Conf libkmldom1 (1.3.0-10 Debian:12.0/stable [amd64])
Conf libkmlengine1 (1.3.0-10 Debian:12.0/stable [amd64])
Conf mysql-common (5.8+1.1.0 Debian:12.0/stable [all])
Conf mariadb-common (1:10.11.3-1 Debian:12.0/stable [all])
Conf libmariadb3 (1:10.11.3-1 Debian:12.0/stable [amd64])
Conf libhdf5-hl-100 (1.10.8+repack1-1 Debian:12.0/stable [amd64])
Conf libnetcdf19 (1:4.9.0-3+b1 Debian:12.0/stable [amd64])
Conf libodbc2 (2.3.11-2 Debian:12.0/stable [amd64])
Conf unixodbc-common (2.3.11-2 Debian:12.0/stable [all])
Conf libodbcinst2 (2.3.11-2 Debian:12.0/stable [amd64])
Conf libogdi4.1 (4.1.0+ds-6 Debian:12.0/stable [amd64])
Conf libpq5 (15.3-0+deb12u1 Debian:12.0/stable [amd64])
Conf libqhull-r8.0 (2020.2-5 Debian:12.0/stable [amd64])
Conf librttopo1 (1.1.0-3 Debian:12.0/stable [amd64])
Conf libspatialite7 (5.0.1-3 Debian:12.0/stable [amd64])
Conf libxerces-c3.2 (3.2.4+debian-1 Debian:12.0/stable [amd64])
Conf libgdal32 (3.6.2+dfsg-1+b2 Debian:12.0/stable [amd64])
Conf libsocket++1 (1.12.13+git20131030.5d039ba-1+b1 Debian:12.0/stable [amd64])
Conf libgdcm3.0 (3.0.21-1 Debian:12.0/stable [amd64])
Conf libjsoncpp25 (1.9.5-4 Debian:12.0/stable [amd64])
Conf liblept5 (1.82.0-3+b3 Debian:12.0/stable [amd64])
Conf libmagick++-6.q16-8 (8:6.9.11.60+dfsg-1.6 Debian:12.0/stable [amd64])
Conf libopencv-flann406 (4.6.0+dfsg-12 Debian:12.0/stable [amd64])
Conf libopencv-features2d406 (4.6.0+dfsg-12 Debian:12.0/stable [amd64])
Conf libopencv-calib3d406 (4.6.0+dfsg-12 Debian:12.0/stable [amd64])
Conf libopencv-dnn406 (4.6.0+dfsg-12 Debian:12.0/stable [amd64])
Conf libqt5opengl5 (5.15.8+dfsg-11 Debian:12.0/stable [amd64])
Conf libopencv-highgui406 (4.6.0+dfsg-12 Debian:12.0/stable [amd64])
Conf libopencv-imgcodecs406 (4.6.0+dfsg-12 Debian:12.0/stable [amd64])
Conf libopencv-ml406 (4.6.0+dfsg-12 Debian:12.0/stable [amd64])
Conf libopencv-objdetect406 (4.6.0+dfsg-12 Debian:12.0/stable [amd64])
Conf libopencv-video406 (4.6.0+dfsg-12 Debian:12.0/stable [amd64])
Conf libtesseract5 (5.3.0-2 Debian:12.0/stable [amd64])
Conf libopencv-contrib406 (4.6.0+dfsg-12 Debian:12.0/stable [amd64])
Conf libopenshot-audio9 (0.3.1+dfsg1-0.1~mx23+1 MX repository:23.0/mx [amd64])
Conf libopenshot23 (0.3.1+dfsg1-0.1~mx23+1 MX repository:23.0/mx [amd64])
Conf libqt5webengine5 (5.15.13+dfsg-1~deb12u1 Debian:12.0/stable [amd64])
Conf python3-sentry-sdk (1.9.10-2 Debian:12.0/stable [all])
Conf python3-defusedxml (0.7.1-2 Debian:12.0/stable [all])
Conf python3-openshot (0.3.1+dfsg1-0.1~mx23+1 MX repository:23.0/mx [amd64])
Conf python3-pyqt5.qtopengl (5.15.9+dfsg-1 Debian:12.0/stable [amd64])
Conf python3-pyqt5.qtsvg (5.15.9+dfsg-1 Debian:12.0/stable [amd64])
Conf python3-pyqt5.qtwebchannel (5.15.9+dfsg-1 Debian:12.0/stable [amd64])
Conf python3-pyqt5.qtwebengine (5.15.6-1 Debian:12.0/stable [amd64])
Conf python3-cffi-backend (1.15.1-5+b1 Debian:12.0/stable [amd64])
Conf python3-py (1.11.0-1 Debian:12.0/stable [all])
Conf python3-zmq (24.0.1-4+b1 Debian:12.0/stable [amd64])
Conf openshot-qt (3.1.0+dfsg1-0.1~mx23+1 MX repository:23.0/mx [all])
root@mx1:/home/demo# apt install openshot-qt

